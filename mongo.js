mongo.js

db.fruits.insertMany(
	[
		{
			"name": "Apple",
			"supplier": "Red Farms Inc.",
			"stocks": 20,
			"price": 40,
			"onSale": true
		},
		{
			"name": "Banana",
			"supplier": "Yellow Farms",
			"stocks": 15,
			"price": 20,
			"onSale": true
		},
		{
			"name": "Kiwi",
			"supplier": "Green Farming and Canning",
			"stocks": 25,
			"price": 50,
			"onSale": true
		},
		{
			"name": "Mango",
			"supplier": "Yellow Farms",
			"stocks": 10,
			"price": 60,
			"onSale": true
		},
		{
			"name": "Dragon Fruit",
			"supplier": "Red Farms Inc.",
			"stocks": 10,
			"price": 60,
			"onSale": true
		}
	]
)

/*
	Aggregation Pipeline Stages

	- is typically done in 2-3 steps. Each process in aggregation is called a stage.
	- db.collection.aggregate()

*/

db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
])



db.fruits.aggregate([

	{$match: {onSale:true}},
	{$group: {_id:null,totalStocks:{$sum:"$stocks"}}}
])

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"RedFarmsInc",totalStocks:{$sum:"$stocks"}}}
])


// mini activity - total stocks on sale by yellow farms

	db.fruits.aggregate([

		{$match: {supplier:"Yellow Farms",onSale:true}},
		{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
	])


	//another solution

	db.fruits.aggregate([

		{$match: {$and: [{supplier:"Yellow Farms"},{onSale:true}]}},
		{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
	])


//$avg - operator used in $group stage

db.fruits.aggregate{[

	{$match:{onSale:true}},
	{$group:{_id:"$supplier", avgStock:{$avg:"$stock"}}}
]}

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:null, avgPrice:{$avg:"$price"}}}
])


//$max - highest number 

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id: "highestStockOnSale", maxStock:{$max:"$stocks"}}}
])

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:null, maxPrice:{$max:"$price"}}}
])


//$min - lowest value

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"lowestStockOnSale", minStock:{$min:"$stocks"}}}
])


db.fruits.aggregate([

	{$match:{onSale:true}},
	{$group:{_id:"lowestPriceOnSale", minPrice:{$min:"$price"}}}
])


// mini-activity get lowest number of stock for all items price lower than 50

	db.fruits.aggregate([

		{$match:{price:{$lt:50}}},
		{$group:{_id:"lowestStock",minStock:{$min:"$stocks"}}}

	])



// $count - a stage added after $match stage to count all items that matches our criteria

db.fruits.aggregate([

	{$match:{onSale:true}},
	{$count:"itemsOnSale"}
])

db.fruits.aggregate([

	{$match:{price:{$lt:50}}},
	{$count:"itemsPriceLessThan50"}
])

db.fruits.aggregate([

	{$match:{stocks:{$lt:20}}},
	{$count:"forRestock"}
])


// $out - save/output the results in a new collection
	//note: will overwrite the collection if already exists


	//result will be number of stock per supplier
	db.fruits.aggregate([

		{$match: {onSale:true}},
		{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}}
	])


	//fetched zero record but creates new collection with result
	db.fruits.aggregate([

		{$match: {onSale:true}},
		{$group: {_id:"$supplier",totalStocks:{$sum:"$stocks"}}},
		{$out: "stocksPerSupplier"}
	])

	// overwrites the collection if out name is same
	db.fruits.aggregate([

		{$match: {onSale:true}},
		{$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}},
		{$out: "stocksPerSupplier"} 
	])








