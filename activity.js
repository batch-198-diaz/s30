//s30 - activity.js



/* 1. COUNT total items supplied by Yellow - price < 50 */

db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms", price:{$lt:50}}},
	{$count:"itemsbyYellowFarms"}
])


/* 2. COUNT total items price < 30 */

db.fruits.aggregate([

	{$match: {price:{$lt:30}}},
	{$count:"itemsLessThan30"}
])


/* 3. AVERAGE price supplied by Yellow */ 

db.fruits.aggregate([

	{$match: {supplier:"Yellow Farms"}},
	{$group:{_id:"YellowFarmAveragePrice", AvgPrice:{$avg:"$price"}}}
])


/* 4. HIGHEST price supplied by Red */

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"MostExpensivebyRedFarm",MaxPrice:{$max:"$price"}}}
])


/* 5. LOWEST price supplied by Red */

db.fruits.aggregate([

	{$match: {supplier:"Red Farms Inc."}},
	{$group: {_id:"CheapestbyRedFarm",MinPrice:{$min:"$price"}}}
])

